CAGI 2018 Regulation Saturation post-challenge analysis
=======

This repository contains the source code and data of the autosome.ru CAGI 2018 "Regulation Saturation" post-challenge analysis.

## Notebook 

The source code is organized in the iPython notebook **model.ipynb**.

The first section in this notebook (Test CAGI) reproduces AUCROC and AUPRC scores for the CAGI test data, the second one (Test ALDOB/ECR11) computes scores for ALDOB and ECR11 reporters [PMID:22371081]. 

The third section (Submission) produces the submission file in CAGI2018 "Regulation Saturation" challenge format. 

The last section (Submission metrics) processes the submission file in the format mentioned before and produces a set of metrics used in the paper.


## Features

The features are available in "Downloads" as 7z-archives (one per features set). They should be unpacked and placed in "features" subdirectory:

* **motifs.7z** (contains motifs_final.tsv)
* **atac.7z** (contains atac_norm_feat.tsv)
* **dnase.7z** (contains dnase_NORM.tsv)
* **gtrd.7z** (containsgtrd_plus_NORMED_NEW.tsv)
* **deepsea.7z** (contains deepsea_all_new.tsv)
* **deepsea_fake.7z** (contains deepsea_fake_all.tsv)

Templates files for challenge data (joined with ALDOB/ECR11 data) are also available in "Downloads". 

They should be placed in "data" subdirectory. 

## Data files 

Template files:

* **submission_example.txt** - template for the CAGI submission file
* **train_template.tsv** - template for the train data
* **test_template.tsv** - template for the test (validation) data
* **all_template.tsv** - template for the file with complete data (test+train)

**aldobecr11.tsv** contains info about ALDOB and ECR11 reporters (with filled confidence and expression change values).

## Data requirements 

The code requires challenge data (should be downloaded from the CAGI website under CAGI agreement separately, see https://genomeinterpretation.org/content/expression-variants) to be filled in all_template.tsv (replace _x.x_ with the actual confidence and expression change values).

